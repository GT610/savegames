# Savegames

My own game save data

## Games included
Game name|Folder name|Status
:-:|:-:|:-:
PC Building Simulator 2|`PCBS2`|Not maintained until next game update
A-Train 9 version 5.0|`A9v5`|No longer maintained
NS games|`NS`|Active

## License
CC-BY-SA 4.0 International